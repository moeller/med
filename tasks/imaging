Task: Imaging
Description: Debian Med image processing and visualization packages
 This metapackage will install Debian packages which might be useful in
 medical image processing and visualization.
 .
 On one hand, it installs several packages supporting various image file
 formats and image management, like DICOM (Digital Imaging and
 Communications in Medicine) which is the de-facto standard for medical
 image management, and NIFTI. On the other hand, it provides a variety of
 software packages that can be used for visualization and for image processing
 - either from a graphical user interface, the command  line, or implemented in
 workflows.

Recommends: amide, ctsim, ctn, dicomnifti, imagej, minc-tools, medcon, python-nibabel, python-dicom, xmedcon, dcmtk

Suggests: imagemagick, pngquant, imview, trimage

Recommends: nifti-bin

Recommends: aeskulap

Recommends: fsl

Recommends: fslview
Remark: fslview was removed from Debian (see #647810)
 The bug log (http://bugs.debian.org/647810) explains the reasons for
 the removal.  The maintainers did never respond whether they plan
 to bring back a more recent version of this package

Recommends: via-bin
Pkg-URL: http://snapshot.debian.org/package/via/2.0.4-2.1/
WNPP: 732129
Comment: removed from Debian and only available in Wheezy or snapshot.debian.org
 The package was removed from Debian since it was blocking the lesstif
 transition.  A very similar library is provided by the mia package
 (see bug #732129).

Recommends: gwyddion

Recommends: sofa-apps

Recommends: python-mvpa2
Why: Suggested by maintainer Michael Hanke <michael.hanke@gmail.com>

Recommends: python-nipy
Why: Although listed in -dev task, it also has a strong focus on interactive
 data analysis.

Recommends: python-nipype
Why: Although listed in -dev task, it also has a strong focus on interactive
 data analysis.

Recommends: python-nitime
Why: Although listed in -dev task, it also has a strong focus on interactive
 data analysis.

Recommends: caret

Recommends: libgdcm-tools

Recommends: slicer
Remark: slicer was removed from Debian (see #644352)
 The bug log (http://bugs.debian.org/644352) explains the reasons for
 the removal and the conditions when a new version might be back

Recommends: lipsia
Remark: lipsia was removed from Debian (see #674295)
 The bug log (http://bugs.debian.org/674295) explains the reasons for
 the removal and the conditions when a new version might be back

Recommends: dicom3tools

Recommends: imagevis3d

Recommends: odin
Published-Title: ODIN: Object-oriented development interface for NMR
Published-Authors: Thies H. Jochimsen, Michael von Mengershausen
Published-In: Journal of Magnetic Resonance 170:67-78
Published-Year: 2004
Published-URL: http://od1n.sourceforge.net/odin.pdf

Recommends: dicomscope

Recommends: pixelmed-apps

Recommends: vmtk

Recommends: ginkgocadx

Recommends: openslide-tools

Recommends: conquest-common

Suggests: conquest-dbase, conquest-mysql, conquest-postgres, conquest-sqlite

Suggests: paraview

Recommends: camitk-imp

Recommends: crea

Recommends: orthanc

Suggests: orthanc-webviewer, orthanc-dicomweb, orthanc-imagej, orthanc-postgresql

Recommends: teem-apps

Recommends: bioimagesuite
Remark: Contact to upstream
 There is a forum at BioImage Suite site for discussion of
 compiling it from source and packaging issues at
 http://research.yale.edu/bioimagesuite/forum/index.php?board=12.0

Recommends: drjekyll
Homepage: http://drjekyll.sourceforge.net
License: GPL
Pkg-Description: interactive voxel editor for viewing and editing three-dimensional images
 It is specifically aimed at postprocessing of segmented datasets,
 but offers some functionality for raw data as well.
 Voxel elements (=voxels) and pixel ("picture element") are viewed
 as data sets and can be processed by this program as kind of
 a final polishing process.
Why: Hint given by Petter Reinholdtsen

Recommends: libdcm4che-java
Remark: Needs jai_imageio as prerequsite
 Building the package using the packaging code in SVN works up to a point
 were jai_imageio.jar is required.  For the status of packaging this
 prerequisite see the Debian Java mailing list at
 http://lists.debian.org/debian-java/2011/04/msg00045.html

Recommends: dcm4chee
Homepage: http://www.dcm4che.org/
License: LGPL, MPL, Apache, other (also non-free)
Pkg-Description: Clinical Image and Object Management (enterprise)
 Contained within the dcm4che project is dcm4chee (the extra 'e'
 stands for 'enterprise'). dcm4chee is an Image Manager/Image Archive
 (according to IHE). The application contains the DICOM, HL7 services
 and interfaces that are required to provide storage, retrieval, and
 workflow to a healthcare environment. dcm4chee is pre-packaged and
 deployed within the JBoss application server. By taking advantage of
 many JBoss features (JMS, EJB, Servlet Engine, etc.), and assuming the
 role of several IHE actors for the sake of interoperability, the
 application provides many robust and scalable services.

Recommends: piano
Homepage: http://mbi.dkfz-heidelberg.de/mbi/software/
License: BSD
Pkg-Description: medical image processing library for surgical planning
 Piano is a library containing roughly 75 algorithms and tools for
 multi-dimensional medical image processing, analysis and visualization.
 It is used in the field of surgical planning.

Recommends: mesa-test-tools
Homepage: http://ihedoc.wustl.edu/mesasoftware/
License: free
Pkg-Description: IHE Test Software for Radiology
 The MESA software release which is available at
 http://ihedoc.wustl.edu/mesasoftware/10.15.0/dist/ provides several
 tools that might cover a wide range of applications for
 Integrating the Healthcare Enterprise (IHE) testing.
 .
 Another important element of the IHE testing process is the set of
 software tools HIMSS and RSNA have commissioned. Developed by the
 Electronic Radiology Laboratory at the Mallinckrodt Institute of
 Radiology, Washington University of St. Louis, the MESA tools are
 designed for use by participating companies in implementing IHE
 capabilities in their systems and preparing for the Connectathon. Their
 purpose is to provide communication partners, test data and test plans
 to allow organizations to provide a baseline level of testing as they
 implement the IHE Technical Framework. These tools are made available to
 participants during the period of an IHE demonstration year and are then
 released into the public domain at the end of that cycle. The latest
 version of the MESA Test Tools available in the public domain can be
 found here.
 .
 This kind of software is definitively valuable for information systems
 vendors and imaging systems vendors.
 .
 Because the CTN Debian package is based on an upstream dead project
 these tools should have a high priority for packaging because the
 CTN homepage http://erl.wustl.edu/research/dicom/ctn.html says:
 "The CTN software is also embedded within the MESA tools. The version
 of CTN software in those tools does not have a separate release number
 but is more current than version 3.0.6."

Recommends: devide
Homepage: http://code.google.com/p/devide/
License: BSD
WNPP: 509110
Responsible: Mathieu Malaterre <mathieu.malaterre@gmail.com>
Pkg-Description: Delft Visualization and Image processing Development Environment
 DeVIDE, or the Delft Visualization and Image processing Development
 Environment, is a Python-based dataflow application builder that enables
 the rapid prototyping of medical visualization and image processing
 applications via visual programming. In other words, by visually connecting
 functional blocks (think Yahoo pipes), you can create cool visualizations.
 .
 See the DeVIDE website at http://visualisation.tudelft.nl/Projects/DeVIDE

Recommends: dicom4j
Homepage: http://dicom4j.sourceforge.net/
License: GPL
Pkg-Description: Java framework for Dicom
 Java framework for Dicom

Recommends: opendicom.net
Homepage: http://opendicom.sourceforge.net/
License: LGPL
Responsible: Albert Gnandt <agnandt@users.sourceforge.net>
Pkg-URL: http://ubuntu.mi.hs-heilbronn.de/other/opendicom
Pkg-Description: API to DICOM in C# for Mono
 The openDICOM.NET project implements a new approach towards DICOM
 (Digital Imaging and Communications in Medicine) libraries. DICOM is
 a worldwide standard in Medical IT and is provided by the National
 Electrical Manufacturers Association (NEMA). This standard specifies
 the way medical images and meta data like study or patient related
 data is stored and communicated over different digital medias. Thus,
 DICOM is a binary protocol and data format.
 .
 The openDICOM# Class Library, main part of the openDICOM.NET project,
 provides an API to DICOM in C# for Mono and the .NET Framework. It is
 a completely new implementation of DICOM. In contrast to other
 similar libraries the intention of this implementation is to provide
 a clean classification with support of unidirectional DICOM data
 streaming. Another implemented goal is the support of DICOM as
 XML. This is not standard conform but very use- and powerful within
 software development, storage and manipulation. Currently, full read
 support of DICOM output stream and full write support to XML is
 supposed to be provided. The entire DICOM content can be accessed as
 sequence or as tree of class instances. Latter is the default
 representation of DICOM content by the library.
 .
 The openDICOM.NET Utils are a collection of console tools for working
 with the needed data dictionaries in different data formats (binary
 and textual), query of ACR-NEMA (prior DICOM standard) and DICOM
 files and transcoding them into image formats like JPEG and XML
 files. These utils are written in C# for Mono and the .NET Framework
 and are using the openDICOM# API for processing.
 .
 The openDICOM.NET Navigator recapitulates the openDICOM.NET Utils in
 form of a GTK# GUI. It provides different views with focus on DICOM
 data sets and visualization. Connectivity to GIMP is also given for
 single image processing purpose as well as the possibility to run
 through multi-frame images like a movie.
 .
 The openDICOM.NET Beagle Filter Plugin increases the usability of
 ACR-NEMA and DICOM query within your desktop. It makes DICOM content
 overall indexable for retrieval. The Beagle search engine relies on
 Mono/.NET and works in the background of your system, but is able to
 detect content changes in realtime (depending on your configuration).
 .
 All GUI applications focus the popular GNOME desktop, but are 100%
 platform independent by relying on Mono.

Recommends: afni
Pkg-URL: http://neuro.debian.net/pkgs/afni.html

Recommends: blox
Homepage: http://sourceforge.net/projects/blox/
License: GPL
Pkg-Description: medical imaging and visualization program
 The purpose of the project is to develop a quantitative medical
 imaging and visualization program for use on brain MR, DTI and MRS
 data. It is a joint project of the Kennedy Krieger Institute and the
 Johns Hopkins University, Psychiatric Neuroimaging Lab
 (http://pni.med.jhu.edu/methods/morph.htm).


Recommends: ecg2png
X-Homepage-old: http://www.cardiothink.com/downloads
Homepage: http://www.freshports.org/graphics/ecg2png/
License: GPL
Pkg-Description: convert scanned electrocardiograms into PNG format
 This program is designed to convert scanned 12-lead
 electrocardiograms into PNG format and a web-friendly image size. It
 assumes that the electrocardiogram (ECG) is printed with a black line
 on white paper with a red grid.
 .
 The problems this program is designed to solve are (1) an ECG scanned
 at relatively high resolution (300 to 600 dots per inch) imposes a
 substantial load on the web browser because it contains about 6
 million pixels which may require 18 to 24 MB of RAM to store for
 display. Also, (2) typical scanners convert a clean paper ECG into a
 multitude of colors, include green and blue. The resulting file
 cannot be compressed efficiently because it does not contain as much
 redundancy, and thus takes more time to transmit over low-speed
 network connections.
Remark: Homepage vanished
 The homepage of this project that used to be at
 http://www.cardiothink.com/downloads/ecg2png/ vanished but the source
 can be downloaded fro instance from
 http://www.freshports.org/graphics/ecg2png/ .

Recommends: kradview

Recommends: opensourcepacs
Homepage: http://www.mii.ucla.edu/index.php/MainSite:OpenSourcePacsHome
License: GPL
WNPP: 509113
Responsible: Mathieu Malaterre <mathieu.malaterre@gmail.com>
Pkg-Description: medical image referral, archiving, routing and viewing system
 OpenSourcePACS is a free, open source image referral, archiving,
 routing and viewing system. It adds functionality beyond conventional
 PACS by integrating wet read functions, implemented through DICOM
 Presentation State and Structured Reporting standards.
 .
 In its first release, OpenSourcePACS delivers a complete wet read
 system, enabling an imaging clinic or hospital to offer its services
 over the web to physicians within or outside the institution. In
 future releases, we hope to incorporate more RIS (dictation,
 transcription, and reporting) functionality.
 .
 OpenSourcePACS is a product of the UCLA Medical Imaging Informatics
 group (http://www.mii.ucla.edu/).

Suggests: visit
Homepage: http://www.llnl.gov/visit/
WNPP: 395573
License: 3-clause BSD license with additional disclaimers
Pkg-Description: visualization and graphical analysis tool for viewing scientific data
 VisIt is a free interactive parallel visualization and graphical
 analysis tool for viewing scientific data.  Users can quickly
 generate visualizations from their data, animate them through time,
 manipulate them, and save the resulting images for presentations.
 VisIt contains a rich set of visualization features so that you can
 view your data in a variety of ways.  It can be used to visualize
 scalar and vector fields defined on two- and three-dimensional (2D
 and 3D) structured and unstructured meshes.
 .
 VisIt was designed to handle very large data set sizes in the terascale
 range and yet can also handle small data sets in the kilobyte range.

Recommends: mni-autoreg
Homepage: http://www.bic.mni.mcgill.ca/software/mni_autoreg/
License: no-free, but GPLed parts
Responsible: NeuroDebian Team <team@neuro.debian.net>
Pkg-URL: http://apsy.gse.uni-magdeburg.de/debian/pool/contrib/m/mni-autoreg-model/
Pkg-Description: MNI average brain (305 MRI) stereotaxic registration model
 This package provides a version of the MNI Average Brain (an average of 305
 T1-weighted MRI scans, linearly transformed to Talairach space) specially
 adapted for use with the MNI Linear Registration Package.
 .
  * average_305.mnc - a version of the average MRI that covers the whole brain
    (unlike the original Talairach atlas), sampled with 1mm cubic voxels
  * average_305_mask.mnc - a mask of the brain in average_305.mnc
  * average_305_headmask.mnc - another mask, required for nonlinear mode

Recommends: mni-n3
Homepage: http://www.bic.mni.mcgill.ca/software/N3/
License: BSDish
Responsible: NeuroDebian Team <team@neuro.debian.net>
Pkg-URL: http://mentors.debian.net/debian/pool/main/m/mni-n3/
Pkg-Description: MNI Non-parametric Non-uniformity Normalization
 MNI Non-parametric Non-uniformity Normalization (N3). This package provides
 the 'nu_correct' tool for unsupervised correction of radio frequency (RF)
 field inhomogenities in MR volumes. Two packages are provided:
  * mni-n3 - provides 'nu_correct'
  * libebtks-dev - MNI support library with numerical types and algorithms

Recommends: brainvisa
Homepage: http://brainvisa.info/
License: Free? (CeCill License)
Pkg-Description: image processing factory for MR images
 BrainVISA is a software, which embodies an image processing
 factory. A simple control panel allows the user to trigger some
 sequences of treatments on series of images. These treatments are
 performed by calls to command lines provided by different
 laboratories. These command lines, hence, are the building blocks on
 which are built the assembly lines of the factory. BrainVISA is
 distributed with a toolbox of building blocks dedicated to the
 segmentation of T1-weighted MR images. The product of the main
 assembly line made up from this toolbox is the following: grey/white
 classification for Voxel Based Morphometry, Meshes of each hemisphere
 surface for visualization purpose, Spherical meshes of each
 hemisphere white matter surface, a graph of the cortical folds, a
 labeling of the cortical folds according to a nomenclature of the
 main sulci.

Recommends: maris
Homepage: http://maris.homelinux.org/
License: GPL
Pkg-Description: package suite for Radiological Workflow
 The MARiS Project goal is to realize a package suite for Radiological
 Workflow using Open Source tools and technologies in according with
 IHE guidelines. The architecture of the single packages is based on
 the concept of IHE actor: this is very useful to develop a system
 that is an ensemble of single pieces that cooperate together using
 IHE profiles.

Recommends: micromanager
Pkg-URL: http://mentors.debian.net/package/micromanager
Remark: Partially problematic licenses
 Unfortunately there is a pile of dirty licenses involved so I'm not sure
 this is ready for Debian yet.  Some of the code cannot be given out and
 some drivers require kernel modules to be built. You have to sign NDAs
 to get access to all the code.

Suggests: mrisim
Homepage: http://packages.bic.mni.mcgill.ca/tgz/
Responsible: NeuroDebian Team <team@neuro.debian.net>
License: BSD-like
Pkg-Description: simulator for magnetic resonance imaging data
 mrisim is a simple Magnetic Resonance Imaging (MRI) simulation program
 which produces MINC volumes from a segmented and labelled brain phantom.
 It allows intrinsic tissue parameters (T1, T2...) and pulse sequence
 parameters (TR, TE ...) to be specified and then produces simulated
 images with noise. Currently, no artifacts are implemented.

Recommends: fiji
Homepage: http://pacific.mpi-cbg.de/
Responsible: Mark Longair <mark-debianlists@longair.net>
Pkg-URL: http://pacific.mpi-cbg.de/wiki/index.php/Downloads
Pkg-Description: The Fiji image processing suite (based on ImageJ)
 Fiji is a project aiming at simplifying:
  * the installation of ImageJ
  * the usage of ImageJ
  * the usage of specific, powerful ImageJ plugins
  * the development of plugins using ImageJ
Remark: About packaging status the authors said:
 See the thread on the Debian Med mailing list at:
 http://lists.debian.org/debian-med/2009/04/msg00059.html
  - We've been working to get rid of, or replace, any remaining
    non-DFSG licensed plugins, but there's at least one left.
    The bug for tracking this is here:
      http://pacific.mpi-cbg.de/cgi-bin/bugzilla/show_bug.cgi?id=19
  - At the moment Fiji depends on sun-java6 rather than openjdk.
  - The packages are rather large at the moment (about 35MiB).
    This is mostly due to bundling various components with Fiji
    that could be satisfied as dependencies in Debian, such as
    junit, jruby, etc. but I haven't had time to work on
    separating those out.
  - Fiji uses a modified version of ImageJA (which again is
    bundled into the fiji package) rather than depending on the
    imagej Debian package created by people on this list.
  - One of the aims of Fiji was to make Benjamin Schmid's
    Java3D-based 3D viewer plugin work out-of-the-box, since
    people often had trouble installing it manually.  At the time
    when I first made these packages there were no java3d
    packages in Debian, but now that these are in sid we should
    eventually be able to switch to using those.

Recommends: cdmedicpacs
Homepage: http://cdmedicpacsweb.sourceforge.net/
License: GPL2
Pkg-URL: http://sourceforge.net/projects/cdmedicpacsweb/files/
Pkg-Description: web interface to PACS to access DICOM study images
 Web based PACS (Picture Archiving and Communication System) to access DICOM
 studies images, in an easy and quick manner.
 .
  * Easy configuration of PACS nodes (AE,IP,Port) , status, statistics
    of storage form web interface.
  * Dynamic web page generation from DICOM data +- prospective preparation
    when system is idle.
  * Automatic video(mp4/gif) generation from Heart MRI at Patient’s heart
    bit frame rate and XA at 15 fps.
  * Still images in jpeg with Window/Level from DICOM header when present
    or with Histogram algorithm.
  * Possibility on large Studies of still images to make a single mp4 for
    Series instead of a bunch of jpeg.
  * Easy web deletion of Series/Studies and  auto deletion (Study date/DB
    insertion) for temporal PACS.
  * Good DICOM interaction with Diagnostic Modalities and commercial DICOM
    Viewers/Work Stations.
  * Good DICOM interaction with free DICOM Viewers Aeskulap

Recommends: stir
Homepage: http://stir.sourceforge.net/
License: GPL
Pkg-Description: Software for Tomographic Image Reconstruction
 STIR is Open Source software for use in tomographic imaging. Its aim is to
 provide a Multi-Platform Object-Oriented framework for all data manipulations
 in tomographic imaging. Currently, the emphasis is on (iterative) image
 reconstruction in PET, but other application areas and imaging modalities
 can and might be added.
 .
 STIR is the successor of the PARAPET software library which was the result
 of a (European Union funded) collaboration between 6 different partners,
 the PARAPET project..
Remark: Even if this is GPLed software the download requires registration.

Recommends: openelectrophy
Pkg-URL: http://neuro.debian.net/pkgs/openelectrophy.html

Recommends: invesalius
X-Screenshot: http://dl.dropbox.com/u/4053278/invesalius_trac/screenshots/invesalius3_promed_0446_bone.png
 This screenshot is done on iOSX :-(


Comment: Several related R packages are listed at CRAN:
         http://cran.r-project.org/web/views/MedicalImaging.html


Recommends: mricron
Published-Title: Improving lesion-symptom mapping
Published-Authors: Chris Rorden, Hans-Otto Karnath, Leonardo Bonilha
Published-In: Journal of Cognitive Neuroscience, 19: 1081-1088
Published-Year: 2007
Published-URL: http://www.ncbi.nlm.nih.gov/pubmed/17583985

Recommends: voxbo

Recommends: mrtrix
Published-Title: Robust determination of the fibre orientation distribution in diffusion MRI: Non-negativity constrained super-resolved spherical deconvolution
Published-Authors: J-Donald Tournier, Fernando Calamantea, Alan Connelly
Published-In: NeuroImage, 35: 1459-1472
Published-Year: 2007
Published-DOI: 10.1016/j.neuroimage.2007.02.016


Recommends: ants
Published-Title: The optimal template effect in hippocampus studies of diseased populations
Published-Authors: Brian B. Avants, Paul Yushkevich, John Pluta, David Minkoff, Marc Korczykowski, John Detre and James C. Gee
Published-In: NeuroImage, 49: 2457-2466
Published-Year: 2010
Published-DOI: 10.1016/j.neuroimage.2009.09.062
X-Institution: UCLA Center for Computation Biology


Recommends: itksnap

Recommends: mriconvert

Recommends: mia-tools, mialmpick, mia-viewit

Recommends: ismrmrd-tools

 ; Added by blends-inject 0.0.7. [Please note here if modified manually]
Suggests: connectomeviewer
Published-Authors: Gerhard S, Daducci A, Lemkaddem A, Meuli R, Thiran J-P and Hagmann P
Published-DOI: 10.3389/fninf.2011.00003
Published-In: Front. Neuroinform. 5:3
Published-Title: The Connectome Viewer Toolkit: An open source framework to manage, analyze, and visualize connectomes
Published-Year: 2011

 ; Added by blends-inject 0.0.7. [Now official package]
Recommends: sigviewer

Recommends: python-tifffile

Recommends: mni-icbm152-nlin-2009
Homepage: http://www.bic.mni.mcgill.ca/ServicesAtlases/ICBM152NLin2009
License: custom, DFSG-compliant
Responsible: NeuroDebian Team <team@neuro.debian.net>
Pkg-Description: MNI stereotaxic space human brain template
 This is an unbiased standard magnetic resonance imaging template volume for
 the normal human population. It has been created by the Montreal Neurological
 Institute (MNI) using anatomical data from the International Consortium for
 Brain Mapping (ICBM).
 .
 The package provides a 1x1x1 mm and 0.5x0.5x0.5 mm resolution templates
 (hemissphere-symetric and asymetric non-linearily co-registered versions),
 some including T1w, T2w, PDw modalities, T2 relaxometry, and tissue probability
 maps. In addition, it contains a lobe atlas, and masks for brain, eyes and
 face.
Published-Authors: V.S. Fonov, A.C. Evans, R.C. McKinstry, C.R. Almli and D.L. Collins
Published-Title: Unbiased nonlinear average age-appropriate brain templates from birth to adulthood.
Published-In: NeuroImage, 47, Supplement 1: 102
Published-Year: 2009
Remark: This package is waiting for the Debian data package archive to become available.


Recommends: mni-colin27-nifti
Pkg-URL: http://neuro.debian.net/pkgs/mni-colin27-nifti.html

Recommends: mipav
Pkg-URL: http://neuro.debian.net/pkgs/mipav.html

Recommends: jist

Recommends: openmeeg-tools

Recommends: jemris
License: GPL-2+
Responsible: NeuroDebian Team <team@neuro.debian.net>
WNPP: 590469
Homepage: http://www.jemris.org/
Pkg-Description: high performance computing MRI simulator
 JEMRIS, which stands for "Juelich Extensible MRI Simulator", is
 a general simulator of MRI (Magnetic Resonance Imaging) data.
 The general process of simulation consists of preparation by choice or
 implementation of sequence, sample and coil setup and the invocation
 of the simulation run itself.
Published-Authors: Tony Stöcker, Kaveh Vahedipour, Daniel Pflugfelder, N. Jon Shah
Published-Title: High-performance computing MRI simulations
Published-In: Magnetic Resonance in Medicine
Published-Year: 2010
Published-DOI: 10.1002/mrm.22406

Recommends: insightapplications
Language: C++, Python, Tcl

Recommends: tempo
Homepage: http://code.google.com/p/tempo/
License: BSD
Language: C++, Qt
Pkg-Description: 3D visualization of brain electrical activity
 TEMPO is open source software for 3D visualization of brain electrical
 activity. TEMPO accepts EEG file in standard EDF format and creates
 animated sequence of topographic maps. Topographic maps are generated
 over 3D head model and user is able to navigate around head and examine
 maps from different viewpoints. Most mapping parameters are adjustable
 through appropriate graphical user interface controls. Also, individual
 topographic maps could be saved in PNG format for future examination or
 publishing.
Remark: Packaged for OpenSuSE http://en.opensuse.org/TEMPO

Recommends: dti-query
Homepage: http://graphics.stanford.edu/projects/dti/software/index.html
License: MIT
Language: C++
Pkg-Description: dynamic queries of the white matter brain pathways
 This application allows neuroscientists to place and interactively manipulate
 box-shaped regions (or volumes of interest) to selectively display pathways
 that pass through specific anatomical areas. A simple and extensible query
 language allows for arbitrary combinations of these queries using Boolean
 logic operators. Queries can be further restricted by numerical path
 properties such as length, mean fractional anisotropy, and mean curvature.
Remark: Depends on RAPID library that is available under non-commercial
 licensing terms.

 ; Added by blends-inject 0.0.5. [Please note here if modified manually]
Suggests: eeglab
Homepage: http://sccn.ucsd.edu/eeglab
Language: C, Matlab/Octave
WNPP: 605739
Responsible: NeuroDebian Team <team@neuro.debian.net>
License: GPL-2+
Pkg-Description: toolbox for processing and visualization of electrophysiological data
 EEGLAB is an interactive Matlab toolbox for processing continuous and
 event-related EEG, MEG and other electrophysiological data
 incorporating independent component analysis (ICA), time/frequency
 analysis, artifact rejection, event-related statistics, and several
 useful modes of visualization of the averaged and single-trial data.
Published-Authors: Delorme A and Makeig S
Published-In: Journal of Neuroscience Methods 134:9-21
Published-Title: EEGLAB: an open source toolbox for analysis of single-trial EEG dynamics
Published-Year: 2004
Registration: http://sccn.ucsd.edu/eeglab/install.html

Suggests: elastix

Suggests: python-pyxid
Recommends: python-dipy
Why: Although listed in -dev task, it also has a strong focus on interactive
 data analysis.

Recommends: plastimatch

Recommends: medisnap
Homepage: http://medisnap.sourceforge.net/
License: GPL-3
Pkg-Description: photograph, manage, view, compare, document and archive medical photos
 Photograph, manage, view, compare, document and archive medical photos fully integrated
 into doctor's practice systems. Take a photo and immediately see how the picture gets
 archived to your current patient automatically.
  * direct support for Olympus E-System cameras
  * network support
  * fully integrated via GDT interface into many medical software systems
  * organise photos by patients effectively
  * define your own localisations
  * compare photos of healing processes at different times
  * work time-optimized and effective, photos automatically get added and archived under
    the current patient in your system
  * easily print selected photos and archive or give them to your patients

Recommends: gimias
Homepage: http://www.gimias.org/
X-SF-URL: http://gimias.sourceforge.net
License: BSD-like
Pkg-Description: Graphical Interface for Medical Image Analysis and Simulation
 GIMIAS is a workflow-oriented environment for solving advanced
 biomedical image computing and individualized simulation problems, which
 is extensible through the development of problem-specific plug-ins. In
 addition, GIMIAS provides an open source framework for efficient
 development of research and clinical software prototypes integrating
 contributions from the Physiome community while allowing
 business-friendly technology transfer and commercial product
 development.
X-Comment: See http://lists.debian.org/debian-med/2011/02/msg00031.html
 Dependencies:
 ANN/ BOOST-1.45.0/ CGNS/ cmakeMacros/ CMGUI/ CXXTEST/ DCMTK-3.5.4/ EXPAT/
 HDF5/ ITK-3.20/ LIBELF/ LOG4CPLUS/ MITK_SVN2/ NETGEN-4.5/ PCRE-8.01/ SLICER/
 SLICERAPPS/ TinyXml/ VTK-5.6.1/ WXMATHPLOT-0.1.0/ WXWIDGETS-2.8.10/
 XERCES-3.0.1/ ZLIB/

 ; Added by blends-inject 0.0.7. [Please note here if modified manually]
Suggests: dtitk
Homepage: http://www.nitrc.org/projects/dtitk
Language: C++
WNPP: 612619
License: GPL-3+
Pkg-Description: DTI spatial normalization and atlas construction toolkit
 DTI-TK is a spatial normalization & atlas construction toolkit,
 designed from ground up to support the manipulation of
 diffusion-tensor images (DTI) with special cares taken to respect the
 tensorial nature of the data. It implements a state-of-the-art
 registration algorithm that drives the alignment of white matter (WM)
 tracts by matching the orientation of the underlying fiber bundle at
 each voxel. The algorithm has been shown to both improve WM tract
 alignment and to enhance the power of statistical inference in
 clinical settings.  The key features include:
 .
  - NIfTI support for scalar, vector and DTI volumes
  - tool chains for manipulating DTI volumes: resampling, smoothing,
    warping, registration & visualization
  - pipelines for WM morphometry: spatial normalization & atlas
    construction for population-based studies
  - built-in cluster-computing support
  - interoperability with other major DTI tools: AFNI, Camino,
    DTIStudio & FSL
Published-Authors: H Zhang, P A Yushkevich, D C Alexander, and J C Gee
Published-DOI: 10.1016/j.media.2006.06.004
Published-In: Medical Image Analysis - Special Issue: The Eighth International
 Conference on Medical Imaging and Computer Assisted intervention - MICCAI 2005,
 10(5):764-785
Published-Title: Deformable registration of diffusion tensor MR images
 with explicit orientation optimization
Published-Year: 2006

Suggests: openwalnut-qt4
Published-Authors: Sebastian Eichelbaum, Mario Hlawitschka, Alexander Wiebel, Gerik Scheuermann
Published-In: Werner Benger et al., editors, Proceedings of 6th High-End Visualization Workshop. Lehmanns
Published-Title: OpenWalnut - An Open-Source Visualization System
Published-Year: 2010

 ; Added by blends-inject 0.0.7. [Please note here if modified manually]
Suggests: miview
Why: Seems to be quite neat
Homepage: http://www.gbooksoft.com/
Language: C++
License: GPL-3+
Pkg-Description: Medical Images viewer and converter
 MIView features
 - DICOM files browser
 - volume rendering
 - reads DICOM v3, NEMA/ACR, Papyrus, Jpeg, GIF, bitmap, TIFF,
   Analyze 7.5, and Nifti1 files
 - can convert to raster (jpeg, bitmap, etc) and Analyze/Nifti1
Remark: At the moment available only for Windows, but author says
 there should be no major showstoppers to build it on Linux -- just
 needs building infrastructure

Recommends: mayam
Language: Java

 ; Added by blends-inject 0.0.7. [extra information removed because package uploaded to Debian]
Suggests: cmtk

Suggests: freesurfer
Remark: The 'tktools' (tkmedit, tksurfer and tkregister2)  under the
  CorTechs license are not readily redistributable thus excluded.
  .
  Here you can see a list where Freesurfer was cited
  http://surfer.nmr.mgh.harvard.edu/fswiki/FreeSurferMethodsCitation

 ; Added by blends-inject 0.0.7. [Please note here if modified manually]
Suggests: xnat
Homepage: http://www.xnat.org
Language: java
WNPP: 629143
License: XNAT SLA
Pkg-Description: platform for data management and productivity tasks in neuroimaging
 The primary functionality of XNAT is to provide a place to store and
 control access to neuroimaging data. This includes sophisticated user
 control, search and retrieval, and archiving capabilities. As
 open-source software, XNAT also supports a wide variety of
 research-based processing pipelines, and is able to link up with
 supercomputer processing power to dramatically shorten image
 processing time.
Published-Authors: Marcus, D.S., Olsen T., Ramaratnam M., and Buckner, R.L.
Published-In: Neuroinformatics 5(1): 11-34
Published-Title: The Extensible Neuroimaging Archive Toolkit (XNAT): An informatics platform for managing, exploring, and sharing neuroimaging data.
Published-Year: 2007
Registration: http://www.xnat.org/download-xnat.html

 ; Added by blends-inject 0.0.7. [Removed unneeded publication data]
Recommends: biosig-tools

 ; Added by blends-inject 0.0.7. [Please note here if modified manually]
Suggests: isis
Why: IO layer for Lipsia and maybe soon Odin.
Homepage: http://isis-group.github.com/isis/
Language: C++
WNPP: 633677
Responsible: NeuroDebian Team <team@neuro.debian.net>
License: GPL-2+
Pkg-Description: I/O framework for neuroimaging data
 This framework aids access of and conversion between various established
 neuro-imaging data formats, like  Nifti, Analyze, DICOM and VISTA. ISIS
 is extensible with plugins to add support for additional data formats.

 ; Added by blends-inject 0.0.7. [Please note here if modified manually]
Suggests: pymeg
Homepage: https://github.com/badbytes/pymeg/wiki
Language: Python
License: GPL-3
Pkg-Description: suite for analysis of magnetoencephalography (MEG) data
  PyMEG is a project in Python to do various neuroimaging processing
  with magnetoencephalography (MEG) data. The purpose of this project,
  is to create a suite of functions to do MEG analysis in Python.
Remark: Needs DFSG-ification.
        According to the author is not yet ready for the use by
        mortals -- wasn't released yet.

 ; Added by blends-inject 0.0.7. [Please note here if modified manually]
Suggests: stabilitycalc
Homepage: https://github.com/bbfrederick/stabilitycalc
Language: Python
Responsible: NeuroDebian Team <team@neuro.debian.net>
License: BSD
Vcs-Git: https://github.com/bbfrederick/stabilitycalc.git
Pkg-URL: http://neuro.debian.net/pkgs/stabilitycalc.html
Pkg-Description: evaluate fMRI scanner stability
 Command-line tools to calculate numerous fMRI scanner stability metrics, based
 on the FBIRN quality assurance test protocal. Any 4D volumetric timeseries
 image in NIfTI format is support input. Output is a rich HTML report.

Recommends: python-surfer

Recommends: dicoogle

Recommends: cellprofiler

Recommends: bioimagexd

Recommends: omero
Homepage: http://www.openmicroscopy.org
License: GPL
Pkg-Description: coming standard LIMS for microscopy images
 OMERO is client-server software for visualisation, management and
 analysis of biological microscope images.

X-Comment: More image viewers might be listed in the Fed-Med project:
 http://fedoraproject.org/wiki/Medical_Imaging

 ; Added by blends-inject 0.0.7. [Please note here if modified manually]
Suggests: hid
Homepage: http://www.nitrc.org/projects/hid
Language: java
License: BSD, BIRN
Pkg-Description: database management system for clinical imaging
 The Human Imaging Database (HID) is an extensible database management
 system developed to handle the increasingly large and diverse
 datasets collected as part of the MBIRN and FBIRN collaboratories and
 throughout clinical imaging communities at large.
Published-Authors: Keator, D.B.; Grethe, J.S.; Marcus, D.; Ozyurt, B.;
 Gadde, S.; Murphy, S.; Pieper, S.; Greve, D.;Notestine, R.; Bockholt,
 H.J.; Papadopoulos, P.
Published-In: IEEE Transactions on Information Technology in
 Biomedicine, 12 (2)
Published-Title: A National Human Neuroimaging Collaboratory Enabled
 By The Biomedical Informatics Research Network (BIRN)
Published-Year: 2008
Registration: http://www.nitrc.org/account/register.php

Recommends: king

Recommends: vtk-dicom-tools

Recommends: gdf-tools

Suggests: incf-nidash-oneclick-clients
Homepage: http://xnat.incf.org/
License: BSD
Language: Python
Pkg-Description: utility for pushing DICOM data to the INCF datasharing server
 A command line utility for anonymizing and sending DICOM data to the XNAT
 image database at the International Neuroinformatics Coordinating Facility
 (INCF). This tool is maintained by the INCF NeuroImaging DataSharing (NIDASH)
 task force.
Responsible: NeuroDebian Team <team@neuro.debian.net>
Vcs-Git: https://github.com/INCF/one_click.git
Vcs-Browser: https://github.com/INCF/one_click

Recommends: fw4spl

Recommends: bart

Recommends: bart-view
WNPP: 851431

Suggests: science-workflow

Recommends: dcm2niix

Recommends: orthanc-wsi
